#!/bin/bash
git checkout main > /dev/null

BRANCH=$(git rev-parse --abbrev-ref HEAD)

if [[ $BRANCH != 'main' ]]; then
    echo "
    não foi possível voltar ao branch main
    "
    exit
fi

git branch $1
git checkout $1

BRANCH=$(git rev-parse --abbrev-ref HEAD)
if [[ $BRANCH != $1 ]]; then
    echo "
    não foi possível criar o branch $1
    "
    exit
fi

VERSION_FILE=$(cat version.txt)

if [ $VERSION_FILE != 'latest' ] && [ $VERSION_FILE != $1 ]; then
    echo "
    o arquivo version.txt deveria estar com o conteúdo latest ou igual a versão sendo criada
    "
    exit
fi

sed -i "s/latest/$1/g" Dockerfile
echo $1 > version.txt
docker buildx build --no-cache --push  --platform linux/arm/v7,linux/arm64/v8,linux/amd64  --tag hacklab/wp:$1 .
git commit Dockerfile version.txt -m "create tag $1"
git push --set-upstream origin $1

