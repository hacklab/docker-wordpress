#!/bin/bash

VERSION=$(<version.txt)
docker build . -t hacklab/wp:$VERSION --no-cache
docker push hacklab/wp:$VERSION