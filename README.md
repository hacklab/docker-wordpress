# docker image `hacklab/wp`

based on wordpress image


## You must have buildx installed and configured in order to build new images
```
docker buildx create --name multiarch --driver docker-container --use
```
 
## creating a new version

```
$ docker login
$ ./create-version.sh 5.8-php7.4
```

## updating the current tag

```
$ docker login
$ ./update.sh
```
